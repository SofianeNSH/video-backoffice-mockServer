const express = require('express');
const app = express();
const cors = require('cors');

const fs = require('fs');

app.use(cors());

const routesMapGet = [
  { route: '/users/:param', file: 'usersId' },
  { route: '/bookmarks', file: 'bookmarks' },
  { route: '/taxonomies/programs', file: 'taxonomiesPrograms' },
];

const routesMapPost = [
  { route: '/users/login', file: 'login' },
  { route: '/taxonomies/search', file: 'taxonomiesSearch' },
];

const addHeaders = (res) => {
  res.setHeader('access-control-expose-headers', 'x-auth-token');
  res.setHeader('content-type', 'application/json');
  res.setHeader('x-content-type-options', 'nosniff');
  res.setHeader('x-auth-token', 'fakeToken');
};

routesMapGet.map((elem) =>
  app.get(elem.route, (req, res) => {
    fs.readFile(__dirname + '/data/' + elem.file + '.json', 'utf8', (err, data) => {
      addHeaders(res);
      res.end(data);
    });
  })
);

routesMapPost.map((elem) =>
  app.post(elem.route, (req, res) => {
    fs.readFile(__dirname + '/data/' + elem.file + '.json', 'utf8', (err, data) => {
      addHeaders(res);
      res.end(data);
    });
  })
);

const server = app.listen(8081, () => {
  const port = server.address().port;
  console.log(`Mocked API STARTED at http://localhost:${port}`);
});
